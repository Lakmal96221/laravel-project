import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import App from './views/app.vue'

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAMfFT8j4qNhtQDRRJtO2AYPjb7tiK3rIw',
    libraries: 'places',
  }
});


new Vue({
  render: h => h(App),
}).$mount('#app')


// import Vue from 'vue'

// //Main pages
// import App from './views/app.vue'


// const app = new Vue({
//     el: '#app',
//     components: { App }
// });
